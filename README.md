# Trying different ML algorithms #

This repo made for collecting different ML projects from course "Machine learning and Intelligent Systems" at EURECOM Fall 2020.

For implementations I am using Jupyter Notebook.


### Lab 1 ###

The aim of this lab is to practice with **linear models** for both **regression** and **classification** via simple experiments.

This lab was completed by me. So now I am able to:

* Interpret the coefficent estimates produced by a linear model

* Become familiar with the use of polynomial and categorical features

* Become familiar with the building blocks of a pipeline to make building, fitting, and tracking models easier

* Make an informed choice of model based on the data at hand

* Understand the key differences between nearest neighbor and linear models



